# Laravel API

Laravel API resource.

Creating a API using Laravel resource rather than using JSON API.

Command line
php artisan make:migration create_articles_table
php artisan make:seeder ArticlesSeeder
php artisan make:factory ArticleFactory
php artisan make:model Article
php artisan make:controller ArticlesController --resource
php artisan migrate

To create a dummy data for the testing purposes
run the seed.
php artisan db:seed 

You can view the API result into postman application.

^__^