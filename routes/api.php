<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//List the Articles
Route::get('articles','ArticlesController@index');
//List single Article
Route::get('article/{id}','ArticlesController@show');
// Create new Article
Route::post('article','ArticlesController@store');
// update Article
Route::put('article','ArticlesController@store');
// Delete Article
Route::delete('article/{id}','ArticlesController@destroy');
